package com.malic.mary.level;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import com.malic.mary.entity.Door;
import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Furniture;
import com.malic.mary.entity.Player;
import com.malic.mary.entity.tile.Tile;
import com.malic.mary.graphics.Screen;
import com.malic.mary.world.World;

public class Level {
	private int ticks;
	private World world;
	private Player player;
	private List<Entity> entities = new ArrayList<Entity>();
	private Door firstDoor = null;
	private int width;
	private int height;
	private int backgroundColour;
	private int level;
	private int timer = 20;
	
	public Level(World world, int level) {
		this.world = world;
		this.level = level;
		backgroundColour = 0xB2FFFF;
	}
	
	public void init(World world) {
		player = world.getPlayer();
		firstDoor.closeDoor();
	}
	
	public Door getFirstDoor() {
		return firstDoor;
	}
	
	/*
	public Level(int width, int height) {
		generateLevel();
	}
	public Level(String path) {
		loadLevel(path);
	}*/

	public void tick() {
		ticks++;
		
		if (player.getPills() == 5) firstDoor.openDoor();
		
		Entity e;
		for (int i = 0; i < entities.size(); i++) {
			e = entities.get(i);
			e.tick();
		}
		for (int i = 0; i < entities.size(); i++) {
			e = entities.get(i);
			if (e.isRemoved()) removeEntity(e);
		}
		
		if (timer == 0) player.die();
		if (ticks % 60 == 0 && timer > 0 && level != 0) timer--;
		
	}

	public void render(Screen screen) {
		screen.fill(backgroundColour);
		for (Entity e : entities) {
			e.render(screen);
		}
	}
	
	public void sort() {
		Player p = null;
		for (Entity e : entities) {
			if (e instanceof Player) {
				p = (Player) e;
			}
		}
		entities.remove(p);
		entities.add(p);
	}
	
	public void addPlayer(int x, int y) {
		if (player == null) player = world.getPlayer();
		player.reset();
		player.setLocation(x, y);
		if (!entities.contains(player))
			addEntity(player);
	}
	
	public void addTile(Tile t) {
		addEntity(t);
	}
	
	public List<Entity> getTiles(Rectangle r) {
		List<Entity> subTiles = new ArrayList<Entity>();
		Tile t;
		for (Entity e : entities) {
			if (e instanceof Tile)
				t = (Tile) e;
			else
				continue;
			if (r.intersects(t.getRect())) {
				subTiles.add(t);
			}
		}
		return subTiles;
	}
	
	public void removeTile(Tile t) {
		if (entities.contains(t)) {
			entities.remove(t);
		}
	}
	
	public void removeTiles(Rectangle r) {
		Tile t;
		for (int i = 0; i < entities.size(); i++) {
			if (entities.get(i) instanceof Tile)
				t = (Tile) entities.get(i);
			else
				continue;
			if (r.intersects(t.getRect())) {
				entities.remove(t);
			}
		}
	}
	
	public void addPill(Entity e) {
		if (entities.contains(e)) return;
		addEntity(e);
	}
	
	public void addEntity(Entity e) {
		if (e instanceof Door) {
			if (firstDoor == null)
				firstDoor = (Door) e;
		}
		entities.add(e);
		e.init(world);
		e.setRemoved(false);
	}
	
	public List<Entity> getEntities(Rectangle r) {
		List<Entity> subEntities = new ArrayList<Entity>();
		for (Entity e : entities) {
			if (r.intersects(e.getRect())) {
				subEntities.add(e);
			}
		}
		return subEntities;
	}
	
	public void removeEntity(Entity e) {
		if (entities.contains(e)) {
			entities.remove(e);
		}
	}
	
	public void removeEntities(Rectangle r) {
		Entity e;
		for (int i = 0; i < entities.size(); i++) {
			e = entities.get(i);
			if (r.intersects(e.getRect())) {
				entities.remove(e);
			}
		}
	}
	
	public void interact(Furniture f, Player p) {
		f.interact(p);
	}
	
	public void clear() {
		entities.clear();
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getTime() {
		return timer;
	}
	
	public void addTime() {
		timer += 10;
	}
	
	public String toString() {
		return "Entities: " + entities.size();
	}

}
