package com.malic.mary;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class InputManager implements KeyListener {
	
	public class Key {
		
		public boolean held = false;
		public boolean pressed = false;
		private long absorbs = 0;
		private long numPressed = 0;
		
		public Key() {
			keys.add(this);
		}
		
		public void tick() {
			if (absorbs < numPressed) {
				absorbs++;
				pressed = true;
			}
			else {
				pressed = false;
			}
		}
		
		public void toggle(Boolean pressed) {
			if (pressed) {
				held = true;
				numPressed++;
			}
			else {
				held = false;
			}
		}
	}
	
	public ArrayList<Key> keys = new ArrayList<Key>();
	
	public Key down = new Key();
	public Key up = new Key();
	public Key left = new Key();
	public Key right = new Key();
	
	public Key escape = new Key();
	public Key pause = new Key();
	public Key enter = new Key();
	public Key jump = new Key();
	
	public InputManager(Game game) {
		game.addKeyListener(this);
	}
	
	public void tick() {
		for (Key k : keys) {
			k.tick();
		}
	}
	
	public void toggle(Boolean pressed, KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_UP) up.toggle(pressed); 
		if (key == KeyEvent.VK_DOWN) down.toggle(pressed);
		if (key == KeyEvent.VK_LEFT) left.toggle(pressed);
		if (key == KeyEvent.VK_RIGHT) right.toggle(pressed);
		
		if (key == KeyEvent.VK_W) up.toggle(pressed); 
		if (key == KeyEvent.VK_S) down.toggle(pressed);
		if (key == KeyEvent.VK_A) left.toggle(pressed);
		if (key == KeyEvent.VK_D) right.toggle(pressed);
		
		if (key == KeyEvent.VK_ESCAPE) escape.toggle(pressed);
		if (key == KeyEvent.VK_P) pause.toggle(pressed); 
		
		if (key == KeyEvent.VK_ENTER) enter.toggle(pressed); 
		if (key == KeyEvent.VK_SPACE) jump.toggle(pressed);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		toggle(true, e);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		toggle(false, e);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
