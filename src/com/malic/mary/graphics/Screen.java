package com.malic.mary.graphics;

import java.awt.image.BufferedImage;

public class Screen {
	
	public int[] image;
	public int[] pixels;
	public int width, w, gridW;
	public int height, h, gridH;
	public final int size = 8;
	
	private int xScroll = 0;
	private int yScroll = 0;
	
	public Screen (BufferedImage tileset, int width, int height) {
		this.width = width;
		this.height = height;
		pixels = new int[width * height];
		
		w = tileset.getWidth();
		h = tileset.getHeight();
		gridW = width/size - 1;
		gridH = height/size - 1;
		image = new int[w*h];
		image = tileset.getRGB(0, 0, w, h, null, 0, w);
		
		init();
	}
	
	public void init() {
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = 0x00FFFF;
		}
		// Remove alpha colour from image pixels
		for (int i = 0; i < image.length; i++) {
			image[i] = Colour.get(image[i]);
		}
	}
	
	public void render(double xOffs, double yOffs, int tile) {
		xOffs -= xScroll;
		yOffs -= yScroll;
		
		int xt = tile % (w/size);
		int yt = tile / (h/size);
		
		for (int x = 0; x < size; x++) {
			if (x + xOffs < 0 || x + xOffs >= width) continue;
			int xx = (int)(xOffs + x);
			
			for (int y = 0; y < size; y++) {
				if (y + yOffs < 0 || y + yOffs >= height) continue;
				int yy = (int)(yOffs + y);
				int pixel = image[(xt * size + x) + (yt * 8 + y) * w];
				if (pixel == 0xFFFFFF) continue;
				
				pixels[(xx + yy*width)] = pixel;
				
			}
			
		}
	}
	
	private String characters = "abcdefghijklmnopqrstuvwxyz    " +
								"0123456789.,!?'\"-+=/\\%()<>:;  ";
	
	public void drawString(String word, int x, int y, int colour) {
		
		word = word.replaceAll("\n\r\t", ""); // deal with later
		word = word.toLowerCase();
		String words[] = word.split(";");

		for (int j = 0; j < words.length; j++) {
			for (int i = 0; i < words[j].length(); i++) {
				int pos = characters.indexOf(words[j].charAt(i));
				if (pos < 30) {
					renderCol(x + i*size + xScroll, y + 12*j + yScroll, pos + 28*30, colour);
				}
				else {
					renderCol(x + i*size + xScroll, y + 12*j + yScroll, pos-30 + 29*30, colour);
				}
			}
		}
	}
	
	public void renderCol(double xOffs, double yOffs, int tile, int colour) {
		xOffs -= xScroll;
		yOffs -= yScroll;
		
		int xt = tile % (w/size);
		int yt = tile / (h/size);
		int col;
		for (int x = 0; x < size; x++) {
			if (x + xOffs < 0 || x + xOffs >= width) continue;
			int xx = (int)(xOffs + x);
			
			for (int y = 0; y < size; y++) {
				col = colour;
				if (y + yOffs < 0 || y + yOffs >= height) continue;
				int yy = (int)(yOffs + y);
				int pixel = image[(xt * size + x) + (yt * 8 + y) * w];
				if (pixel == 0x000000) continue;
				if (pixel == 0x050505) col = 0x050505;
				pixels[(xx + yy*width)] = col;
			}
			
		}
	}

	public void drawTextBox(int x0, int y0, int x1, int y1, int col) {
		if (x1 < x0 || y1 < y0) return;
		//if (x0 < 0) x0 = 0;
		//if (y0 < 0) y0 = 0;
		if (x1 < x0) x1 = x0 + 1;
		if (y1 < y0) y1 = y0 + 1;
		
		int xt, yt;
		
		for (int x = x0; x <= x1; x++) {
			for (int y = y0; y <= y1; y++) {
				if (x == x0 && y == y0) {
					xt = 0;
					yt = 24;
				}
				else if (x == x1 && y == y0) {
					xt = 2;
					yt = 24;
				}
				else if (x == x1 && y == y1) {
					xt = 2;
					yt = 26;
				}
				else if (x == x0 && y == y1) {
					xt = 0;
					yt = 26;
				}
				else if (x == x0) {
					xt = 0;
					yt = 25;
				}
				else if (y == y0) {
					xt = 1;
					yt = 24;
				}
				else if (x == x1) {
					xt = 2;
					yt = 25;
				}
				else if (y == y1) {
					xt = 1;
					yt = 26;
				}
				else {
					xt = 1;
					yt = 25;
				}
				renderCol(x*size + xScroll, y*size + yScroll, xt + yt*30, col);
			}
		}
	}

	@SuppressWarnings("unused")
	private int getTextBoxHeight(String word) {
		int height = 1 + countOccurences(word, "\n");
		for (int i = 0; i < word.length(); i++) {
			if (i%gridW == 0) height++;
		}
		return height;
	}
	
	private int countOccurences(String str, String substring) {
		int count = 0;
		int strlen = str.length();
		int sublen = substring.length();
		for (int i = 0; i < str.length(); i++) {
			if (sublen > strlen) break;
			if (str.substring(i, i + sublen).equals(substring)) count++;
		}
		return count;
	}

	public void fill(int r, int g, int b) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				pixels[x + y * width] = r << 16 | g << 8 | b;
			}
		}
	}
	
	public void fill(int a) {
		int r, g, b;
		r = a >> 16 & 0x0000FF;
		g = a >> 8 & 0x0000FF;
		b = a & 0x0000FF;
		fill(r, g, b);
	}
	
	public void setOffset(int xScroll, int yScroll) {
		this.xScroll = xScroll;
		this.yScroll = yScroll;
	}

	public int getXScroll() {
		return xScroll;
	}
	
	public int getYScroll() {
		return yScroll;
	}
	
}
