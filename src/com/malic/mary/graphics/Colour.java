package com.malic.mary.graphics;

public class Colour {
	
	public static int get(int r, int g, int b) {
		return r << 16 | g << 8 | b;
	}
	
	public static int get(int a) {
		int r, g, b;
		r = a & 0xFF0000;
		g = a & 0x00FF00;
		b = a & 0x0000FF;
		return r | g | b;
	}

}
