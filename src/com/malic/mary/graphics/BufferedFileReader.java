package com.malic.mary.graphics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class BufferedFileReader {
	
	private InputStreamReader isr;
	private BufferedReader br;
	private List<String> level;
	private final String ext = ".txt";
	
	public BufferedFileReader() {
		level = new ArrayList<String>();
	}
	
	public void readFile(String name) throws IOException {
		level.clear();
		InputStream is = this.getClass().getResourceAsStream("/" + name + ext);
		try {
			isr = new InputStreamReader(is);
			br = new BufferedReader(isr);
			
			String line;
			while ((line = br.readLine()) != null) {
				level.add(line);
			}
		} catch (NullPointerException e) {
			System.err.println("Unable to load file: " + name);
		}
		finally {
			isr.close();
			br.close();
		}
	}
	
	public List<String> getFile() {
		return level;
	}

}
