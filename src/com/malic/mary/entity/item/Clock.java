package com.malic.mary.entity.item;

import com.malic.mary.SoundManager;
import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Player;
import com.malic.mary.entity.Text;
import com.malic.mary.graphics.Screen;
import com.malic.mary.world.Statistics;

public class Clock extends Items {
	int t;
	int interval = 10;
	
	public Clock(int x, int y) {
		super(x, y);
		tile = 0 + 7 * 30;
		ticks = 0;
	}
	
	public void tick() {
		super.tick();
		t = 0;
		for (int i = 1; i <= 7; i++) {
			if (ticks >= interval*i) t++;
		}
		
		if (ticks > 80) ticks = 0;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile + t);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		if (e instanceof Player) {
			if (!removed) {
				world.getLevel().addTime();
				Statistics.addTime();
				SoundManager.pickup.play();
				world.getLevel().addEntity(new Text((int)x, (int)y - 16, "+10"));
			}
			
			removed = true;
		}
	}
}
