package com.malic.mary.entity.item;

import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Player;
import com.malic.mary.entity.Text;
import com.malic.mary.graphics.Screen;

public class Heart extends Items {

	public Heart(int x, int y) {
		super(x, y);
		tile = 0 + 9 * 30;
	}
	
	public void tick() {
		super.tick();
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		if (e instanceof Player) {
			if (removed) return;
			removed = true;
			world.getLevel().addEntity(new Text((int)x, (int)y - 16, "Heart"));
			Player p = (Player) e;
			p.addHealth(1);
		}
	}

}
