package com.malic.mary.entity.item;

import java.util.Random;

import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Player;
import com.malic.mary.graphics.Screen;

public class Fruit extends Items {
	private Random rand = new Random();
	public enum type { BANANA, CHERRY, GRAPE, GAPPLE, RAPPLE, ORANGE, AVOCADO, MUSHROOM };
	int fruit;
	
	public Fruit(int x, int y) {
		super(x, y);
		fruit = getFruit().ordinal();
	}
	
	private type getFruit() {
		type fruit = null;
		int num = rand.nextInt() & 1000;
		if (num >= 0) fruit = type.BANANA;
		if (num >= 200) fruit = type.CHERRY;
		if (num >= 400) fruit = type.GRAPE;
		if (num >= 600) fruit = type.GAPPLE;
		if (num >= 800) fruit = type.RAPPLE;
		if (num >= 900) fruit = type.ORANGE;
		if (num >= 950) fruit = type.AVOCADO;
		if (num >= 990) fruit = type.MUSHROOM;
		return fruit;
	}
	
	public void tick() {
		
	}
	
	public void render(Screen screen) {
		screen.render(x, y, fruit);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		if (e instanceof Player) {
			
			if (!removed) {
				removed = true;
				Player p = (Player) e;
				p.addFruit(this);
			}
				
		}
	}
	
	public int getType() {
		return fruit;
	}

}
