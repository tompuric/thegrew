package com.malic.mary.entity.item;

import com.malic.mary.entity.Entity;
import com.malic.mary.graphics.Screen;

public class Items extends Entity {
	
	public Items(int x, int y) {
		this.x = x;
		this.y = y;
		w = h = 8;
	}
	
	public void tick() {
		super.tick();
	}
	
	public void render(Screen screen) {
		
	}
	
	public boolean canPass() {
		return true;
	}
	
}
