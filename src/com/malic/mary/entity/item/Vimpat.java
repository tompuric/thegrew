package com.malic.mary.entity.item;

import com.malic.mary.entity.Entity;
import com.malic.mary.graphics.Screen;

/**
 * Big blue pill
 * @author Tomislav
 *
 */
public class Vimpat extends Pill {

	public Vimpat(int x, int y) {
		super(x, y);
		tile = 1 + 8 * 30;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
	}

	public String toString() {
		return "Vimpat";
	}

}
