package com.malic.mary.entity.item;

import com.malic.mary.entity.Entity;
import com.malic.mary.graphics.Screen;

public class Microgynon extends Pill {

	public Microgynon(int x, int y) {
		super(x, y);
		tile = 4 + 8 * 30;
	}

	public void render(Screen screen) {
		screen.render(x, y, tile);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
	}

	public String toString() {
		return "Microgynon";
	}
}
