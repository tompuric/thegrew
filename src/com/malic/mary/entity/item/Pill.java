package com.malic.mary.entity.item;

import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Player;
import com.malic.mary.entity.Text;

public class Pill extends Items {

	public Pill(int x, int y) {
		super(x, y);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		if (e instanceof Player) {
			if (!removed) ((Player) e).addPill(this);
			removed = true;
			world.getLevel().addEntity(new Text((int)x, (int)y - 16, toString()));
		}
	}

}
