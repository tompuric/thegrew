package com.malic.mary.entity;

import com.malic.mary.graphics.Screen;
import com.malic.mary.menu.TextMenu;

public class Door extends Furniture {
	private int door;
	private boolean closed;
	
	public Door(int x, int y, int door) {
		super(x, y);
		this.door = door;
		xt = 0;
		yt = 21;
		w = h = 16;
		closed = false;
	}
	
	public void tick() {
		
	}
	
	public void render(Screen screen) {
		screen.render(x + 0, y + 0, (xt + 0) + (yt + 0) * 30);
		screen.render(x + 8, y + 0, (xt + 1) + (yt + 0) * 30);
		screen.render(x + 0, y + 8, (xt + 0) + (yt + 1) * 30);
		screen.render(x + 8, y + 8, (xt + 1) + (yt + 1) * 30);
	}
	
	public void interact(Player player) {
		if (closed) {
			world.getGame().setMenu(new TextMenu("Door is closed"));
			return;
		}
		world.changeLevel(door);
		if (door == 0) return;
		closeDoor();
	}
	
	public boolean canPass() {
		return true;
	}
	
	public void closeDoor() {
		if (closed) return;
		closed = true;
		xt = 2;
	}
	
	public void openDoor() {
		closed = false;
		xt = 0;
	}
	
	public int getDoor() {
		return door;
	}

}
