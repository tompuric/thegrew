package com.malic.mary.entity;

import java.util.ArrayList;
import java.util.List;

import com.malic.mary.Game;
import com.malic.mary.InputManager;
import com.malic.mary.SoundManager;
import com.malic.mary.entity.item.Fruit;
import com.malic.mary.entity.item.Items;
import com.malic.mary.entity.tile.Tile;
import com.malic.mary.graphics.Screen;
import com.malic.mary.world.Statistics;

public class Player extends Mob {
	@SuppressWarnings("unused")
	private Game game;
	private InputManager input;
	private int points;
	private int fruits;
	private int pills;
	private boolean invulnerable = false;
	private int invulnerableTimer = 0, dyingTimer = 0;
	private boolean dying = false;
	private List<String> pillsGathered;
	
	public Player(Game game, InputManager input) {
		super(0, 0);
		this.game = game;
		this.input = input;
		pillsGathered = new ArrayList<String>();
		w = 10;
		h = 16;
		yt = 13;
		pills = 0;
		health = 5;
		points = 0;
		fruits = 0;
	}
	
	public void tick() {
		super.tick();
		
		if (dying && !inAir) {
			dyingTimer++;
			if (dyingTimer < 20)
				xt = 10;
			else
				xt = 12;
			if (dyingTimer > 100) world.lost();
			return;
		}
			
		vx = STOP;
		if (input.left.held) vx = LEFT;
		if (input.right.held) vx = RIGHT; 
		
		if (vx == RIGHT) {
			yt = 13;
			
		}
		if (vx == LEFT) {
			yt = 15;
		}
		
		xt = 0;
		if (vx != STOP) {
			if (ticks > 0) xt += 2;
			if (ticks > 15) xt += 2;
		}
	
		if (input.jump.pressed) jump();
		
		if (inAir) {
			if (input.jump.held)
				vy = ((jumpTime*jumpTime) >> 8) - (jumpTime >> 8) - 2.2;
			else
				vy = ((jumpTime*jumpTime) >> 8) - (jumpTime >> 8) - 1.5;
			
			xt = 6;
		}
		else {
			inAir = true;
			jumpTime = 25;
		}
		
		if (input.enter.pressed || input.up.pressed) interact(); 
		
		if (invulnerable) {
			invulnerableTimer++;
			if (invulnerableTimer < 20) {
				xt = 8;
				knockBack();
			}
		}
		if (invulnerableTimer > 60) {
			invulnerable = false;
			invulnerableTimer = 0;
		}
		
		if (y > 250) vx = 0;
		move(vx, vy);
		
		if (health <= 0) die();
		if (y > 1000) {
			x = 50;
			y = 0;
			hurt();
		}
		
		if (ticks > 30) ticks = 0;
		
		
	}
	
	public void render(Screen screen) {
		if (inAir) xt = 6;
		
		screen.render(x + 0 - 3, y + 0, (xt + 0) + (yt + 0) * 30);
		screen.render(x + 8 - 3, y + 0, (xt + 1) + (yt + 0) * 30);
		screen.render(x + 0 - 3, y + 8, (xt + 0) + (yt + 1) * 30);
		screen.render(x + 8 - 3, y + 8, (xt + 1) + (yt + 1) * 30);
	}
	
	public void hurt() {
		if (invulnerable) return;
		health--;
		invulnerable = true;
		SoundManager.playerHurt.play();
	}
	
	private void knockBack() {
		if (yt == 13) { // Facing right
			vx = -0.2;
		}
		if (yt == 15) { // Facing left
			vx = 0.2;
		}
	}
	
	public void die() {
		if (dying) return;
		dying = true;
		
	}
	private void interact() {
		world.interact(this);
	}
	
	public void steppedOn(Tile t, double vx2, double vy2) {
		if (vy > 0 && (y + vy + h >= t.getY()) && vy2 != 0) {
			landed();
			vy = 1;
		}
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		e.touchedBy(this, vx2, vy2);
	}
	
	public void setLocation(int x, int y) {
		super.setLocation(x, y);
		inAir = true;
		jumpTime = 20;
	}
	
	public void addPill(Items i) {
		pills++;
		pillsGathered.add(i.toString());
		SoundManager.pickup.play();
	}
	
	public int getPills() {
		return pills;
	}
	
	public int getHealth() {
		return health;
	}
	
	public void reset() {
		pills = 0;
		pillsGathered.clear();
	}
	
	public void addPoints(int points) {
		this.points += points;
	}
	
	public void addFruit(Fruit f) {
		this.fruits++;
		SoundManager.pickup.play();
		Statistics.addFruit(f.getType());
	
	}
	
	public int getPoints() {
		return points;
	}

	public boolean hasPill(int pill) {
		if (pill < 0 || pill >= 5) return false;
		String p = null;
		switch (pill) {
			case 0:
				p = "Panadol";
				break;
			case 1:
				p = "Vimpat";
				break;
			case 2:
				p = "Alodorm";
				break;
			case 3:
				p = "Keppra";
				break;
			case 4:
				p = "Microgynon";
				break;
		}
		if (pillsGathered.contains(p)) return true;
		return false;
	}

	public void addHealth(int i) {
		SoundManager.pickup.play();
		if (health + i > 5) health = 5;
		else health += i;
	}
	
	public int getFruits() {
		return fruits;
	}

}
