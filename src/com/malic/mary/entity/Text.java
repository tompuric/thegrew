package com.malic.mary.entity;

import com.malic.mary.graphics.Screen;

public class Text extends Entity {
	String text;
	
	public Text(int x, int y, String text) {
		this.x = x;
		this.y = y;
		this.text = text;
	}
	
	public void tick() {
		super.tick();
		if (ticks > 60) removed = true;
		move(0, -0.1);
	}
	
	public void render(Screen screen) {
		screen.drawString(text, (int) x - screen.getXScroll(), (int) y - screen.getYScroll(), 0xF2244);
	}
	
	public boolean canPass() {
		return true;
	}
}
