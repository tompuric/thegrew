package com.malic.mary.entity;

import com.malic.mary.graphics.Screen;

public class Cake extends Mob {
	

	public Cake(int x, int y) {
		super(x, y);
		tile = 9 + 2 * 30;
		w = 16;
		h = 8;
	}
	
	public void tick() {
		super.tick();
		t = 0;
		if (vx > 0) tile = 9 + 3 * 30;
		if (vx < 0) tile = 9 + 2 * 30;
		if (ticks > 20) t = 2;
		if (ticks > 40) ticks = 0;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile + t);
		screen.render(x + 8, y, tile + t + 1);
	}
	
	protected void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
	}

}
