package com.malic.mary.entity;

import com.malic.mary.graphics.Screen;

public class Fries extends Mob {

	public Fries(int x, int y) {
		super(x, y);
		tile = 0 + 2 * 30;
		w = h = 8;
	}
	
	public void tick() {
	}
	
	
	public void render(Screen screen) {
		screen.render(x, y, tile);
	}
	
	protected void touchedBy(Entity e, double vx2, double vy2) {
		if (e instanceof Player) {
			Player p = (Player) e;
			p.hurt();
			removed = true;
		}
	}

}
