package com.malic.mary.entity;

import com.malic.mary.graphics.Screen;

public class Chip extends Mob {

	public Chip(int x, int y) {
		super(x, y);
		tile = 2 + 2 * 30;
		w = h = 8;
	}
	
	public void tick() {
		super.tick();
		t = 0;
		if (vx > 0) tile = 2 + 3 * 30;
		if (vx < 0) tile = 2 + 2 * 30;
		if (ticks > 20) t++;
		if (ticks > 40) t++;
		if (ticks > 60) ticks = 0;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile + t);
	}
	
	protected void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
	}

}
