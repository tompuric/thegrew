package com.malic.mary.entity;

import com.malic.mary.entity.tile.Tile;
import com.malic.mary.graphics.Screen;

public class Icecream extends Mob {

	public Icecream(int x, int y) {
		super(x, y);
		tile = 1 + 2 * 30;
		w = h = 8;
	}
	
	public void tick() {
		super.tick();
		if (ticks % 120 == 0) jump();
		if (vx > 0) tile = 1 + 3 * 30;
		if (vx < 0) tile = 1 + 2 * 30;
		if (inAir) {
			vy = ((jumpTime*jumpTime) >> 8) - (jumpTime >> 8) - 1.5;
		}
		else {
			vy = 1;
		}
	}
	
	
	public void render(Screen screen) {
		screen.render(x, y, tile);
	}
	
	protected void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
	}
	
	public void steppedOn(Tile t, double vx2, double vy2) {
		super.steppedOn(t, vx2, vy2);
		if (vy2 != 0) {
			inAir = false;
		}
		landed();
	}

}
