package com.malic.mary.entity;

import com.malic.mary.graphics.Screen;
import com.malic.mary.menu.TextMenu;

public class Sign extends Furniture {
	
	private String message;

	public Sign(int x, int y, String message) {
		super(x, y);
		w = h = 8;
		xt = 8;
		yt = 4;
		this.message = message;
	}
	
	public void tick() {

	}
	
	public void render(Screen screen) {
		screen.render(x, y, xt + yt * 30);
	}
	
	public void interact(Player p) {
		world.getGame().setMenu(new TextMenu(message));
	}
	
	public boolean canPass() {
		return true;
	}

}
