package com.malic.mary.entity.tile;

import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Mob;
import com.malic.mary.entity.Player;
import com.malic.mary.graphics.Screen;

public class BlockTile extends Tile {

	int xt;
	
	public BlockTile(int x, int y, int level) {
		super(x, y);
		xt = 6;
		switch (level) {
			case 0:
				xt = 6;
				break;
			case 1:
			case 2:
				xt = 2;
				break;
			case 3:
			case 4:
				xt = 3;
				break;
			case 5:
			case 6:
				xt = 4;
				break;
			case 7:
			case 8:
				xt = 5;
				break;
			
		}
		tile = xt + 4 * 30;
	}

	public void tick() {
		
	}
	
	public void render(Screen screen) {
		screen.render(x, y, xt + 4 * 30);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
		if (!(e instanceof Player)) {
			Mob m = (Mob) e;
			if (m.getVY() == 0) 
				m.setVX(-m.getVX());
		}
	}
}
