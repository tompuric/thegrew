package com.malic.mary.entity.tile;

import com.malic.mary.graphics.Screen;
import com.malic.mary.world.World;

public class BedTile extends Tile {
	
	public BedTile(int x, int y) {
		super(x, y + 2);
		w = 16;
		h = 14;
	}
	
	public void init(World world) {
		super.init(world);
		rect.setLocation((int)x, (int)y + 2);
	}
	
	public void tick() {
		
	}
	
	public void render(Screen screen) {
		screen.render(x + 0, y + 0 - 2, 4 + 21 * 30);
		screen.render(x + 8, y + 0 - 2, 5 + 21 * 30);
		screen.render(x + 0, y + 8 - 2, 4 + 22 * 30);
		screen.render(x + 8, y + 8 - 2, 5 + 22 * 30);
	}

}
