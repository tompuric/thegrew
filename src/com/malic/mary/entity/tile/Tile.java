package com.malic.mary.entity.tile;

import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Mob;
import com.malic.mary.entity.Player;
import com.malic.mary.graphics.Screen;
import com.malic.mary.world.World;

public class Tile extends Entity {
	
	public Tile(int x, int y) {
		this.x = x;
		this.y = y;
		w = 8;
		h = 8;
	}
	
	public void init(World world) {
		super.init(world);
	}
	
	public void tick() {
		super.tick();
	}
	
	public void render(Screen screen) {
		
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		
		
		if (e instanceof Mob) {
			Mob m = (Mob) e;
			m.steppedOn(this, vx2, vy2);
			if (m instanceof Player) return;
			if (vy2 != 0) {
				m.landed();			
			}
			else {
				m.setVX(-m.getVX());
			}
		}
	}

}
