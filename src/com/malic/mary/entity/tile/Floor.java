package com.malic.mary.entity.tile;

import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Mob;
import com.malic.mary.entity.Player;
import com.malic.mary.graphics.Screen;

public class Floor extends Tile {
	
	public Floor(int x, int y) {
		super(x, y);
		tile = 0 + 4 * 30;
	}

	public void tick() {
		
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
		if (!(e instanceof Player)) {
			Mob m = (Mob) e;
			if (m.getVY() == 0) 
				m.setVX(-m.getVX());
		}
	}
}
