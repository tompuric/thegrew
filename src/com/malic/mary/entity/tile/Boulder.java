package com.malic.mary.entity.tile;

import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Mob;
import com.malic.mary.entity.Player;
import com.malic.mary.graphics.Screen;

public class Boulder extends Tile {
	boolean activated = false;
	
	public Boulder(int x, int y) {
		super(x, y);
		tile = 9 + 4 * 30;
		ticks = 0;
	}

	public void tick() {
		if (activated) ticks++;
		if (ticks > 60)
			move(0, 1);
		if (y > 150) removed = true;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile);
	}
	
	public void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
		if (!(e instanceof Player)) {
			Mob m = (Mob) e;
			if (m.getVY() == 0) 
				m.setVX(-m.getVX());
		}
		if (e instanceof Player) {
			initTimer();
		}
	}
	
	private void initTimer() {
		if (activated) return;
		activated = true;
	}
}
