package com.malic.mary.entity;

import com.malic.mary.Game;
import com.malic.mary.SoundManager;
import com.malic.mary.entity.tile.Tile;
import com.malic.mary.graphics.Screen;

public class Mob extends Entity {
	protected int t;
	protected double vx;
	protected double vy;
	
	protected int xt;
	protected int yt;
	
	protected boolean inAir = false;
	protected int jumpTime;
	
	protected final int LEFT = -1;
	protected final int RIGHT = 1;
	protected final int STOP = 0;
	
	protected int health = 1;
	
	protected boolean activated = false;
	
	public Mob(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void tick() {
		super.tick();
		if (inAir && jumpTime < 40) jumpTime++;
		
		if (this instanceof Player) return;
		
		if (x < world.getPlayer().getX() + (Game.WIDTH >> 1)) activate();
		if (x < world.getPlayer().getX() - Game.WIDTH) removed = true;
		move(vx, vy);
		if (vy > 250) removed = true;
	}
	
	public void render(Screen screen) {
		
	}

	protected void touchedBy(Entity e, double vx2, double vy2) {
		if (e instanceof Player) {
			Player p = (Player) e;
			if (p.getVY() > 0 && p.getY() + p.getHeight() - h <  y) {
				removed = true;
				SoundManager.enemyHurt.play();
			}
			else
				p.hurt();
		}
	}
	
	public boolean canPass() {
		return true;
	}
	
	public void steppedOn(Tile t, double vx2, double vy2) {
		if (vy > 0 && (y + vy + h >= t.getY())) {
			landed();
			vy = 1;
		}
	}
	
	protected void jump() {
		if (inAir) return;
		inAir = true;
		jumpTime = 0;
	}
	
	public boolean inAir() {
		return inAir;
	}
	
	public int getJumpTime() {
		return jumpTime;
	}
	
	public void landed() {
		jumpTime = 40;
		inAir = false;
	}
	
	public double getVX() {
		return vx;
	}
	
	public double getVY() {
		return vy;
	}
	
	protected void activate() {
		if (activated) return;
		activated = true;
		vx = -0.5;
		vy = 1.5;
	}

	public void setVX(double vx) {
		this.vx = vx;
	}

}
