package com.malic.mary.entity;

import com.malic.mary.graphics.Screen;

public class CupCake extends Mob {

	public CupCake(int x, int y) {
		super(x, y);
		tile = 7 + 2 * 30;
		w = h = 8;
	}
	
	public void tick() {
		super.tick();
		t = 0;
		if (ticks > 20) t++;
		if (ticks > 40) ticks = 0;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile + t);
	}
	
	protected void touchedBy(Entity e, double vx2, double vy2) {
		super.touchedBy(e, vx2, vy2);
	}

}
