package com.malic.mary.entity;

import java.awt.Rectangle;
import java.util.List;

import com.malic.mary.graphics.Screen;
import com.malic.mary.world.World;

public class Entity {
	protected World world;
	protected Rectangle rect;
	protected double x;
	protected double y;
	protected int w;
	protected int h;
	protected int tile;
	protected short ticks;
	protected boolean removed;
	
	public void init(World world) {
		this.world = world;
		rect = new Rectangle((int)x, (int)y, w, h);
	}
	
	public void tick() {
		ticks++;
	}
	
	public void render(Screen screen) {
		
	}
	
	public void move(double vx, double vy) {
		move2(vx, 0);
		move2(0, vy);
	}
	
	private boolean move2(double vx, double vy) {
		if (vx != 0 && vy != 0) System.err.println("Cannot move in two directions at the same time");

		List<Entity> wasInside = world.getEntities(new Rectangle((int)x, (int)y, w, h));
		List<Entity> isInside = world.getEntities(new Rectangle((int)(x + vx), (int)(y + vy), w, h));
		
		for (Entity e : isInside) {
			if (e == this) continue;
			e.touchedBy(this, vx, vy);
		}
		
		isInside.removeAll(wasInside);
		
		for (Entity e : isInside) {
			if (e == this) continue;
			if (!e.canPass()) return false;
		}
		
		x += vx;
		y += vy;
		rect.setLocation((int) (x + vx), (int) (y + vy));
		return true;
	}
	
	protected void touchedBy(Entity e, double vx, double vy) {
		
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public int getWidth() {
		return w;
	}
	
	public double getHeight() {
		return h;
	}
	
	public Rectangle getRect() {
		return rect;
	}
	
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean canPass() {
		return false;
	}
	
	public boolean isRemoved() {
		return removed;
	}
	
	public void setRemoved(boolean b) {
		removed = b;
	}

}
