package com.malic.mary;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class GameMain extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String name = "The Legend of Mary Grew";
	public static final int WIDTH = 320*3;
	public static final int HEIGHT = 240*3;
	private Game game;

	public GameMain(String name) {
		super(name);
		game = new Game();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(true);
		
		setLayout(new BorderLayout());
		add(game, BorderLayout.CENTER);
		
		pack();
		game.start();
		
		setSize(WIDTH, HEIGHT);
		setLocationRelativeTo(null);
	}

	public static void main(String[] args) {
		new GameMain(name);
	}

}
