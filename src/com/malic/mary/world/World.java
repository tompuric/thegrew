package com.malic.mary.world;

import java.awt.Rectangle;
import java.io.IOException;
import java.util.List;

import com.malic.mary.Game;
import com.malic.mary.InputManager;
import com.malic.mary.entity.Entity;
import com.malic.mary.entity.Furniture;
import com.malic.mary.entity.Player;
import com.malic.mary.graphics.BufferedFileReader;
import com.malic.mary.graphics.Screen;
import com.malic.mary.level.Level;
import com.malic.mary.menu.LostMenu;
import com.malic.mary.menu.MainMenu;
import com.malic.mary.menu.WonMenu;

public class World {
	private BufferedFileReader bfr;
	private LevelReader levelReader;
	private Game game;
	private InputManager input;
	private final int totalLevels = 9;
	private Level[] levels;
	private Level level;
	private int currentLevel;
	private Player player;
	private int xScroll, yScroll;
	private int levelsClosed;
	private boolean lost = false;
	private boolean won = false;
	
	public World(Game game, InputManager input) {
		this.game = game;
		this.input = input;
		
		bfr = new BufferedFileReader();
		player = new Player(game, input);
		levelReader = new LevelReader(this);
	}
	
	public void init() {
		player.init(this);
		levels = loadLevels();
		won = false;
		lost = false;
		currentLevel = 1;
		changeLevel(0);
	}
	
	private Level[] loadLevels() {
		Level[] lvl = new Level[totalLevels];
		for (int i = 0; i < totalLevels; i++) {
			try {
				bfr.readFile("level" + i);
			} catch (IOException e) {
				e.printStackTrace();
			}
			levelReader.readLevel(bfr.getFile(), i);
			lvl[i] = levelReader.getLevel();
		}
		if (lvl.length == 0) System.out.println("UNABLE TO LOAD LEVELS");
		return lvl;
	}
	
	public void changeLevel(int lvl) {
		if (currentLevel == 0 && lvl == 0) won();
		level = levels[lvl];
		level.init(this);
		level.addPlayer(0, 50);
		currentLevel = lvl;
		if (currentLevel == 0) {
			levelsClosed++;
			if (levelsClosed == totalLevels) level.getFirstDoor().openDoor();
		}
	}

	public void tick() {
		if (input.escape.pressed || input.pause.pressed) game.setMenu(new MainMenu());
		
		if (lost) game.setMenu(new LostMenu());
		if (won) game.setMenu(new WonMenu());

		xScroll = (int) ((int)player.getX() - (Game.WIDTH >> 1));
		yScroll = 0;
		if (player.getY() < 60)
			yScroll = (int) (player.getY() - 60);
		int end = level.getWidth() - Game.WIDTH;
		
		if (xScroll < 0) xScroll = 0;
		if (xScroll > end) xScroll = end;
		
		if (player.getX() < 0) player.setX(0);
		if (player.getX() > level.getWidth() - player.getWidth()) player.setX(level.getWidth() - player.getWidth());
		
		level.tick();
	}
	
	public void render(Screen screen) {
		screen.setOffset(xScroll, yScroll);
		level.render(screen);
		
		// UI Backgorund
		screen.drawTextBox(10, -1, 19, 3, 0x00FFAA);
		screen.drawTextBox(20, -1, 29, 3, 0x00FFAA);
		screen.drawTextBox(30, -1, 39, 3, 0x00FFAA);
		
		// Pills UI
		for (int i = 0; i < 5; i++) {
			int col = 0x00FFAA;
			if (currentLevel != 0) {
				if (player.getPills() == 5) {
					col = 0xFFAA00;
					screen.drawString("unlocked", 8, 16, 0x13642C);
				}
				else {
					screen.drawString(" locked", 8, 16, 0xFF2244);
				}
			}
			screen.drawTextBox(i*2, 0, 1 + i*2, 1, col);
			if (player.hasPill(i)) screen.render((4 + i*16) + xScroll, 4 + yScroll, i + 8 * 30);
		}
		
		// Fruit Basket
		screen.render(88 + xScroll, 4 + yScroll, 8 + 21 * 30);
		screen.render(96 + xScroll, 4 + yScroll, 9 + 21 * 30);
		screen.render(88 + xScroll, 12 + yScroll, 8 + 22 * 30);
		screen.render(96 + xScroll, 12 + yScroll, 9 + 22 * 30);
		
		// Points from fruits
		screen.drawString("  : " + player.getFruits(), 88, 8, 0);
		
		// Health UI
		screen.drawString("HP: ", 168 , 8, 0);
		for (int i = 0; i < 5; i++) {
			if (i < player.getHealth())
				screen.render(192 + (i*8 + 2) + xScroll, 7 + yScroll, 0 + 9 * 30);
			else
				screen.render(192 + (i*8 + 2) + xScroll, 7 + yScroll, 1 + 9 * 30);
		}
		
		// Time Remaining UI
		screen.drawString("Time: ", 248, 8, 0);
		if (level.getTime() <= 10)
			screen.drawString("      " + level.getTime(), 248, 8, 0xFF2244);
		else
			screen.drawString("      " + level.getTime(), 248, 8, 0x4422FF);
	}
	
	private void won() {
		won = true;
	}
	
	public void lost() {
		lost = true;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Level getLevel() {
		return level;
	}

	public List<Entity> getEntities(Rectangle r) {
		return level.getEntities(r);
	}
	
	public List<Entity> getTiles(Rectangle r) {
		return level.getTiles(r);
	}
	
	public void interact(Player p) {
		List<Entity> ents = getEntities(p.getRect());
		ents.remove(p);
		Furniture f = null;
		for (Entity e : ents) {
			if (e instanceof Furniture) f = (Furniture) e;
		}
		if (f == null) return;
		else level.interact(f, p);
	}
	
	public int getXScroll() {
		return xScroll;
	}
	
	public int getYScroll() {
		return yScroll;
	}
	
	public Game getGame() {
		return game;
	}
}
