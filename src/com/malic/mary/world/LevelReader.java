package com.malic.mary.world;

import java.util.ArrayList;
import java.util.List;

import com.malic.mary.entity.Cake;
import com.malic.mary.entity.Chip;
import com.malic.mary.entity.Cookie;
import com.malic.mary.entity.CupCake;
import com.malic.mary.entity.Door;
import com.malic.mary.entity.Fries;
import com.malic.mary.entity.Icecream;
import com.malic.mary.entity.Sign;
import com.malic.mary.entity.item.Alodorm;
import com.malic.mary.entity.item.Clock;
import com.malic.mary.entity.item.Fruit;
import com.malic.mary.entity.item.Heart;
import com.malic.mary.entity.item.Keppra;
import com.malic.mary.entity.item.Microgynon;
import com.malic.mary.entity.item.Panadol;
import com.malic.mary.entity.item.Vimpat;
import com.malic.mary.entity.tile.BedTile;
import com.malic.mary.entity.tile.BlockTile;
import com.malic.mary.entity.tile.Boulder;
import com.malic.mary.entity.tile.Dirt;
import com.malic.mary.entity.tile.Floor;
import com.malic.mary.level.Level;

public class LevelReader {
	private World world;
	private Level level;
	private int doors;
	private int pills;
	private int size = 8;
	private List<String> messages;
	private int currentMessage;
	public LevelReader(World world) {
		this.world = world;
	}
	
	public void readLevel(List<String> lvl, int i) {
		level = new Level(world, i);
		messages = new ArrayList<String>();
		currentMessage = 0;
		int xMax = 0;
		doors = 0;
		pills = 0;
		for (int y = 0; y < lvl.size(); y++) {
			String line = lvl.get(y);
			if (line.startsWith("//")) continue;
			if (line.startsWith("!")) {
				messages.add(line.substring(1));
				continue;
			}
			xMax = Math.max(xMax, line.length());
			for (int x = 0; x < line.length(); x++) {
				addInstance(line.charAt(x), x, y - messages.size(), size, i);
			}
		}
		level.sort();
		level.setHeight(lvl.size());
		level.setWidth(xMax*size);
		
		if (i != 0 && pills != 5) {
			System.err.println("Level loaded without all pills");
		}
	}
	
	private void addInstance(char c, int x, int y, int z, int i) {
		x = x*z;
		y = y*z;
		switch(c) {
			case '0': // BlockTile
				level.addTile(new BlockTile(x, y, i));
				break;
			case 'b': // BedTile
				level.addTile(new BedTile(x, y));
				break;
			case 'p': // Player
				level.addPlayer(x, y);
				break;
			case 'c': // Clock
				level.addEntity(new Clock(x, y));
				break;
			case 'f': // Fruit
				level.addEntity(new Fruit(x, y));
				break;
			case 'd': // Door
				level.addEntity(new Door(x, y, doors));
				doors++;
				break;
			case '1': // Panadol
				level.addPill(new Panadol(x, y));
				pills++;
				break;
			case '2': // Vimpat
				level.addPill(new Vimpat(x, y));
				pills++;
				break;
			case '3': // Alodorm
				level.addPill(new Alodorm(x, y));
				pills++;
				break;
			case '4': // Keppra
				level.addPill(new Keppra(x, y));
				pills++;
				break;
			case '5': // Microgynon
				level.addPill(new Microgynon(x, y));
				pills++;
				break;
			case '!': // Fries
				level.addEntity(new Fries(x, y));
				break;
			case '@': // Icecream
				level.addEntity(new Icecream(x, y));
				break;
			case '#': // Chip
				level.addEntity(new Chip(x, y));
				break;
			case '$': // Cookie
				level.addEntity(new Cookie(x, y));
				break;
			case '%': // Cupcake
				level.addEntity(new CupCake(x, y));
				break;
			case '^': // Cake
				level.addEntity(new Cake(x, y));
				break;
			case 's': // Sign
				level.addEntity(new Sign(x, y, messages.get(currentMessage)));
				currentMessage++;
				break;
			case 'h': // Health
				level.addEntity(new Heart(x, y));
				break;
			case 'A': // Floor
				level.addTile(new Floor(x, y));
				break;
			case 'B': // DIRT
				level.addTile(new Dirt(x, y));
				break;
			case 'C': // BOULDER
				level.addTile(new Boulder(x, y));
			default:
				break;
		}
	}
	
	public Level getLevel() {
		return level;
	}

}
