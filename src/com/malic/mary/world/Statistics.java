package com.malic.mary.world;

public class Statistics {
	//public enum type { BANANA, CHERRY, GRAPE, GAPPLE, RAPPLE, ORANGE, AVOCADO, MUSHROOM };
	public static int fruits[] = new int[8];
	public static int time;
	
	public Statistics() {
		
	}

	public static void addFruit(int i) {
		fruits[i]++;
	}
	
	public static void addTime() {
		time++;
	}
	
	public static void reset() {
		for (int i = 0; i < fruits.length; i++) {
			fruits[i] = 0;
		}
		time = 0;
	}
}
