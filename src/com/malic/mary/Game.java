package com.malic.mary;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.malic.mary.graphics.Screen;
import com.malic.mary.menu.Menu;
import com.malic.mary.menu.StartMenu;
import com.malic.mary.world.Statistics;
import com.malic.mary.world.World;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = 1L;
	
	// CORE GAME VARIABLES
	public final static int WIDTH = 320;
	public final static int HEIGHT = 144;
	public volatile boolean running = false;
	
	// GRAPHIC VARIABLES
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	private BufferedImage tileset;
	private Screen screen;
	
	// ENGINE VARIABLES
	private int ticks = 0;
	private int frames = 0;
	private final int sleepTime = 2;
	private long startTime;
	
	// Game Variables
	private World world;
	private InputManager input;
	private Menu menu;
	private int fps = 0;
	private int tps = 60;
	

	public Game() {
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		init();
	}
	
	public void init() {
		
		try {
			tileset = ImageIO.read(this.getClass().getResourceAsStream("/MarySet.png"));
		} catch (IOException e) {
			System.err.println("Failed to load image");
		}
		screen = new Screen(tileset, WIDTH, HEIGHT);
		input = new InputManager(this);
		resetGame();
	}
	
	public void resetGame() {
		world = new World(this, input);
		world.init();
		Statistics.reset();
		setMenu(new StartMenu());		
	}
	
	public void setMenu(Menu menu) {
		this.menu = menu;
		if (menu != null) {
			
			menu.init(this, input);
		}
	}

	@Override
	public void run() {
		startTime = System.currentTimeMillis();
		double nsPerTick = 1000000000.0/tps; // 1/60 of a second
		double preprocessed = 0.0;
		long lastTime = System.nanoTime();
		while(running) {
			
			long now = System.nanoTime();
			/*
			 * 'now - lastTime' results in how long the entire game (including the sleepTime) 
			 * executes for. It is then divided by 'nsPerTick'. This value determines if the program
			 * needs to catch up on or slow down on it's refresh rate. In order to do this correctly,
			 * you have to keep incrementing the preprocessed value with these results otherwise it'll
			 * never tick (because the computer processes the game loop too quickly or will only tick
			 * a few times. 
			 */
			preprocessed += (now - lastTime)/nsPerTick;
			lastTime = now;
			
			while (preprocessed >= 1) {
				tick();
				preprocessed--;
			}
			
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			render();
			
			// if one second has passed, calculate averages
			if (System.currentTimeMillis() - startTime > 1000) {
				fps = frames;
				tps = ticks;
				System.out.println(" fps: " + fps + "\ttps: " + tps);
				frames = 0;
				ticks = 0;
				startTime = System.currentTimeMillis();
			}
			
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void tick() {
		ticks++;
		input.tick();
		
		if (menu != null) {
			menu.tick();
		}
		else {
			world.tick();
		}
	}

	private void render() {
		frames++;
		// Creates the Buffer for each frame being displayed (creates 3 buffers)
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			requestFocus();
			return;
		}
		
		draw(screen); // draws the screen graphics manipulating the pixels (need to make class now)
		
		// Grabs the graphics of these Buffers (from images) to be ready for processing
		Graphics g = bs.getDrawGraphics();
		// Fills the screen with a blank colour
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// Takes measurements for exact dimensions
		float modifier = Math.min(getWidth()/(float)WIDTH, getHeight()/(float)HEIGHT);
		
		int w = (int) (WIDTH*modifier);
		int h = (int) (HEIGHT*modifier);
		
		// Draws the game on the screen from the buffer
		g.drawImage(image, (getWidth() - w)/2, (getHeight() - h)/2, w, h, null);
		g.dispose();
		bs.show();
	}
	
	public void draw(Screen screen) {
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen.pixels[i];
			//screen.pixels[i] = 0xFFFFFF;
		}
		
		if (menu != null) {
			menu.render(screen);
		}
		else {
			world.render(screen);
		}
		
		//screen.drawString("FPS: " + fps, 1, 1);
		//screen.drawString("TPS: " + tps, 73, 1);
		
	}

	public void start() {
		new Thread(this).start();
		running = true;
	}
	
	public void stop() {
		running = !running;
	}
	
	public World getWorld() {
		return world;
	}

}
