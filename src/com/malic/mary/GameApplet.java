package com.malic.mary;

public class GameApplet {
	
	private Game game;
	
	public void init() {
		game = new Game();
	}
	
	public void start() {
		game.start();
	}
	
	public void stop() {
		game.stop();
	}
}
