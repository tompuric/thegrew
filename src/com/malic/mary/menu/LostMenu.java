package com.malic.mary.menu;

import com.malic.mary.graphics.Screen;

public class LostMenu extends Menu {
	
	public void tick() {
		if (input.enter.pressed) game.resetGame(); 
	}
	
	public void render(Screen screen) {
		screen.fill(0, 0, 0);
		
		screen.drawString("Game Over", 120, 20, 0xFFFFFF);
		screen.drawString("I'm sorry to say that you're now late.", 10, 40, 0xFFFFFF);
		screen.drawString("There are consequences when you arrive;late to events", 10, 60, 0xFFFFFF);
		screen.drawString("Perhaps next time?", 10, 90, 0xFFFFFF);
		
		
		
		screen.drawString("Press 'enter' to restart", 60, 120, 0xFFFFFF);
	}

}
