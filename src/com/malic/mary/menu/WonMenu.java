package com.malic.mary.menu;

import com.malic.mary.graphics.Screen;

public class WonMenu extends Menu {
	private int ticks;
	private int ticks2;
	private int xt;
	private int x = 300;
	
	public void tick() {
		ticks++;
		ticks2++;
		if (input.enter.pressed && ticks > 273) game.resetGame(); 
		xt = 0;
		if (ticks2 >= 0) xt += 2;
		if (ticks2 >= 15) xt += 2;
		
		if (ticks < 273) x--;
		if (ticks2 > 30) ticks2 = 0;
		if (ticks > 273) xt = 0;
	}
	
	public void render(Screen screen) {
		screen.fill(0, 0, 0);
		screen.drawString("Game Won", 50, 20, 0xFFFFFF);
		
		if (ticks > 350) screen.drawString("Happy 21st Mary Grew. I love you :)", 20, 50, 0xFFFFFF);
		if (ticks > 350) screen.drawString("       - Tomislav Puric", 100, 70, 0xFFFFFF);
		
		
		// Bed
		screen.render(0 + screen.getXScroll(), 114 + screen.getYScroll(), 4 + 21 *30);
		screen.render(8 + screen.getXScroll(), 114 + screen.getYScroll(), 5 + 21 *30);
		screen.render(0 + screen.getXScroll(), 122 + screen.getYScroll(), 4 + 22 *30);
		screen.render(8 + screen.getXScroll(), 122 + screen.getYScroll(), 5 + 22 *30);
		
		// Tomislav
		screen.render(20 + screen.getXScroll(), 114 + screen.getYScroll(), 0 + 11 *30);
		screen.render(28 + screen.getXScroll(), 114 + screen.getYScroll(), 1 + 11 *30);
		screen.render(20 + screen.getXScroll(), 122 + screen.getYScroll(), 0 + 12 *30);
		screen.render(28 + screen.getXScroll(), 122 + screen.getYScroll(), 1 + 12 *30);
		
		// Mary
		screen.render(x + screen.getXScroll(), 114 + screen.getYScroll(), xt + 15 *30);
		screen.render(x + 8 + screen.getXScroll(), 114 + screen.getYScroll(), xt + 1 + 15 *30);
		screen.render(x + screen.getXScroll(), 122 + screen.getYScroll(), xt + 16 *30);
		screen.render(x + 8 + screen.getXScroll(), 122 + screen.getYScroll(), xt + 1 + 16 *30);
		
		for (int i = 0; i < 40; i++) {
			screen.render(0 + i * 8, 130, 0 + 4*30);
			screen.render(0 + i * 8, 138, 1 + 4*30);
		}
		if (ticks > 100) screen.render(24 + screen.getXScroll(), 100 + screen.getYScroll(), 9*30);
		if (ticks > 300) screen.render(35 + screen.getXScroll(), 100 + screen.getYScroll(), 9*30);
	}

}
