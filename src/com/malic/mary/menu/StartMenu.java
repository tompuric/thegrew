package com.malic.mary.menu;

import com.malic.mary.SoundManager;
import com.malic.mary.graphics.Screen;

public class StartMenu extends Menu {
	
	private String title = "The Legend of Mary Grew";
	private String[] list = {"Start Game", "How To Play", "About", "Exit"};
	private String message;
	private int selected;
	
	public void tick() {
		super.tick();
		if (input.enter.pressed) {
			SoundManager.select.play();
			game.setMenu(new MainMenu()); 
		}
		if (input.down.pressed) selected++;
		
		if (input.up.pressed) selected--;
		if (selected == list.length) selected = 0;
		if (selected < 0) selected = list.length - 1;
		
		if (input.enter.pressed){
			if (selected == 0) game.setMenu(null);
			if (selected == 1) game.setMenu(new HowToPlayMenu());
			if (selected == 2) game.setMenu(new AboutMenu());
			if (selected == 3) System.exit(0);
		}
	}
	
	public void render(Screen screen) {
		screen.fill(0, 0, 0);
		screen.drawString(title, screen.width/2 - title.length()*4, 30, 0xFFFFFF);
		
		
		for (int i = 0; i < list.length; i++) {
			message = list[i];
			if (selected == i)
				message = "> " + message + " <";
			
			screen.drawString(message, screen.width/2 - message.length()*4, 60 + 10*i, 0xFFFFFF);
		}
		//screen.drawString(">", 12, 50 + 10*selected);
		
		screen.drawString(" Press Enter to Select Current Option.", screen.size, screen.gridH*screen.size, 0xFFFFFF);
	}

}
