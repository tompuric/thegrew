package com.malic.mary.menu;

import com.malic.mary.SoundManager;
import com.malic.mary.graphics.Screen;

public class TextMenu extends Menu {
	
	String message;
	
	public TextMenu(String message) {
		this.message = message;
	}
	
	public void tick() {
		if (input.enter.pressed || input.up.pressed) {
			game.setMenu(null); 
			SoundManager.select.play();
		}
	}
	
	public void render(Screen screen) {
		screen.drawTextBox(8, 6, 32, 10, 0x3F48CC);
		screen.drawString(message, 9 * 8, 7 * 8, 0);
	}

}
