package com.malic.mary.menu;

import com.malic.mary.SoundManager;
import com.malic.mary.graphics.Screen;

public class HowToPlayMenu extends Menu {
	
	String messages[] = { "Arrow Keys / A + D: moves",
						  "Space bar: Jumps",
						  "Escape: Pauses",
						  "Enter/Up/W: Interacts"
						  
	};
	
	public void tick() {
		if (input.escape.pressed || input.enter.pressed) {
			game.setMenu(new StartMenu()); 
			SoundManager.select.play();
		}
	}
	
	public void render(Screen screen) {
		screen.fill(0, 0, 0);
		for (int i = 0; i < messages.length; i++) {
			screen.drawString(messages[i], screen.width/2 - messages[i].length()*4, 50 + i*10, 0xFFFFFF);
		}
		
	}

}
