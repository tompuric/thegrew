package com.malic.mary.menu;

import com.malic.mary.graphics.Screen;
import com.malic.mary.world.Statistics;

public class MainMenu extends Menu {
	int ticks = 0, t;
	
	public void tick() {
		super.tick();
		if (input.pause.pressed) game.setMenu(null); 
		ticks++;
		t = 0;
		for (int i = 1; i <= 7; i++) {
			if (ticks >= 10*i) t++;
		}
		if (ticks > 80) ticks = 0;
	}
	
	public void render(Screen screen) {
		screen.drawTextBox(0, 0, 9, 17, 0x0000FF);
		screen.drawTextBox(30, 0, 39, 17, 0x0000FF);
		screen.drawString("Fruits", 8, 16, 0xFFFFFF);
		for (int i = 0; i < 8; i++) {
			screen.render(16 + screen.getXScroll(), 30 + 14*i + screen.getYScroll(), i);
			screen.drawString(" x " + Statistics.fruits[i], 24, 30 + 14*i, 0xFFFFFF);
		}
		screen.drawString("Time;Collected", 245, 16, 0xFFFFFF);
		screen.render(248 + screen.getXScroll(), 48 + screen.getYScroll(), t + 7 * 30);
		screen.drawString("x " + Statistics.time, 262, 48, 0xFFFFFF);
		screen.drawString("Time;Remaining", 245, 70, 0xFFFFFF);
		screen.render(248 + screen.getXScroll(), 100 + screen.getYScroll(), t + 7 * 30);
		screen.drawString(": " + game.getWorld().getLevel().getTime(), 262, 100, 0xFFFFFF);
		
		screen.drawString("Paused", screen.width/2 - 24, 50, 0);
	}
	
}
