package com.malic.mary.menu;

import com.malic.mary.Game;
import com.malic.mary.InputManager;
import com.malic.mary.SoundManager;
import com.malic.mary.graphics.Screen;

public class Menu {
	protected Game game;
	protected InputManager input;
	
	public void init(Game game, InputManager input) {
		this.game = game;
		this.input = input;
	}
	
	public void tick() {
		
		if (input.escape.pressed) {
			SoundManager.select.play();
			game.setMenu(null);
		}
	}
	
	public void render(Screen screen) {
		
	}
}
