package com.malic.mary.menu;

import com.malic.mary.SoundManager;
import com.malic.mary.graphics.Screen;

public class AboutMenu extends Menu {
	
	public void tick() {
		if (input.escape.pressed || input.enter.pressed) {
			SoundManager.select.play();
			game.setMenu(new StartMenu()); 
		}
	}
	
	public void render(Screen screen) {
		screen.fill(0, 0, 0);
		screen.drawString("About Menu", 20, 20, 0x00FFFF);
		screen.drawString("Welcome player, you have entered the", 20, 50, 0xFFFFFF);
		screen.drawString("life of Mary Grew and have just", 20, 60, 0xFFFFFF);
		screen.drawString("woken up. Your goal is to escape", 20, 70, 0xFFFFFF);
		screen.drawString("your house before you'll become late", 20, 80, 0xFFFFFF);
		screen.drawString("to your event. Help Mary get out and;be there on time.", 20, 90, 0xFFFFFF);
		screen.drawString("Press enter to continue.", 20, 136, 0xFFFFFF);
	}

}
