package com.malic.mary;

import java.applet.Applet;
import java.applet.AudioClip;

public class SoundManager {
	public static final SoundManager playerHurt = new SoundManager("/playerhurt.wav");
	public static final SoundManager enemyHurt = new SoundManager("/hit.wav");
	public static final SoundManager select = new SoundManager("/test.wav");
	public static final SoundManager pickup = new SoundManager("/pickup.wav");

	private AudioClip clip;

	private SoundManager(String name) {
		try {
			clip = Applet.newAudioClip(SoundManager.class.getResource(name));
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void play() {
		try {
			new Thread() {
				public void run() {
					clip.play();
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	public void loop() {

		try {
			new Thread() {
				public void run() {
					clip.loop();
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	public void stop() {
		try {
			new Thread() {
				public void run() {
					clip.stop();
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}